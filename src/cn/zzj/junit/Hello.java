package cn.zzj.junit;

public class Hello
{
    public String hello()
    {
        return "world";
    }

    public String nil()
    {
        return null;
    }

    public String notNil()
    {
        return "notnull";
    }

    public void exception()
    {
        throw new NullPointerException();
    }
}