package cn.zzj.test;

import static junit.framework.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cn.zzj.junit.Hello;

public class TestHello
{
    private Hello h;

    @Before
    public void setUp() throws Exception
    {
        System.out.println("start");
        h = new Hello();
    }

    @Test
    public void testHello()
    {
        String str = h.hello();
        assertEquals("不为world", str, "world");
    }

    @Test
    public void testNil()
    {
        String str = h.nil();
        assertNull("不为空", str);
    }

    @Test
    public void testNotNil()
    {
        String str = h.notNil();
        assertNotNull("为空", str);
    }

    @Test
    public void testException()
    {
        try
        {
            h.exception();
            fail("未抛出异常");
        }
        catch (Exception e)
        {
        }
    }

    @After
    public void tearDown() throws Exception
    {
        h = null;
        System.out.println("end");
    }
}